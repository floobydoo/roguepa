﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour
{
    public Canvas MainCanvas;
    public Canvas OptionsCanvas;
    public Canvas SoundCanvas;
    public Canvas Instructions;
    public Canvas Texture;
    public Canvas DifficultyCanvas;
    public Canvas HUD;

    void Start()
    {
        DifficultyCanvas.enabled = false;
        OptionsCanvas.enabled = false;
        SoundCanvas.enabled = false;
        Instructions.enabled = false;
        Texture.enabled = false;
        bool b = GameController.Instance.NeedsMenu();
        bool m = GameController.Instance.NeedsHUD();
        MainCanvas.enabled = b;
        HUD.enabled = m;
    }

    public void OptionsOn()
    {
        DifficultyCanvas.enabled = false;
        OptionsCanvas.enabled = true;
        MainCanvas.enabled = false;
        Instructions.enabled = false;
        Texture.enabled = false;
    }

    public void ReturnOn()
    {
        DifficultyCanvas.enabled = false;
        OptionsCanvas.enabled = false;
        MainCanvas.enabled = true;
        Instructions.enabled = false;
        Texture.enabled = false;
    }

    public void LoadOn()
    {
        DifficultyCanvas.enabled = false;
        HUD.enabled = true;
        OptionsCanvas.enabled = false;
        MainCanvas.enabled = false;
        Instructions.enabled = false;
        Texture.enabled = false;
    }

    public void SoundOff()
    {
        DifficultyCanvas.enabled = false;
        SoundController.Instance.music.Stop();
        SoundCanvas.enabled = true;
        OptionsCanvas.enabled = false;
    }
    public void SoundOn()
    {
        DifficultyCanvas.enabled = false;
        SoundCanvas.enabled = false;
        OptionsCanvas.enabled = true;
        Instructions.enabled = false;
        Texture.enabled = false;
    }

    public void HowTo()
    {
        DifficultyCanvas.enabled = false;
        SoundCanvas.enabled = false;
        OptionsCanvas.enabled = false;
        Instructions.enabled = true;
        Texture.enabled = false;
    }
    public void ApplySkin()
    {
        DifficultyCanvas.enabled = false;
        SoundCanvas.enabled = false;
        OptionsCanvas.enabled = false;
        Instructions.enabled = false;
        Texture.enabled = true;
    }
    public void Difficulty()
    {
        DifficultyCanvas.enabled = true;
        SoundCanvas.enabled = false;
        OptionsCanvas.enabled = false;
        Instructions.enabled = false;
        Texture.enabled = false;
    }
}
